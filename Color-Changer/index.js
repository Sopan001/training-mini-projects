const btn = document.querySelector("#btn");


const colorGenerator = () => {
    const hexColor = "0123456789ABCDEF";
    let backgroundColor = "#";
    for(let j=0;j<6;j++)
    {
        const index = Math.floor(Math.random() * 16);
        backgroundColor += hexColor[index]; 
    }
    return backgroundColor;
}


const changeColor = () =>{
    let boxes = document.querySelectorAll(".box");
    boxes.forEach(box =>
        {
            const backgroundColor = colorGenerator();
            box.style.backgroundColor = backgroundColor;
            box.style.borderColor = backgroundColor;
        });
}

btn.addEventListener("click", changeColor);