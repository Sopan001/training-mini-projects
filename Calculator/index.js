const submitBtn = document.getElementById('submit');
const option = document.getElementById("operation");
const num1 = document.getElementById("num1");
const num2 = document.getElementById("num2");
const resultText = document.getElementById("result");

const calculate = () =>{

    const optionsVal = option.value;
    const num1Val = Number(num1.value);
    const num2Val = Number(num2.value);

    let result = 0;

    switch (optionsVal)
    {
        case 'addition':
            result = num1Val + num2Val;
        break;
        case 'subtraction':
            result = num1Val - num2Val;
        break;
        case 'multiplication':
            result = num1Val * num2Val;
        break;
        case 'division':
            result = num1Val / num2Val;
        break;

    }

    resultText.innerText = result;

}


submitBtn.addEventListener("click",calculate);