const pageMain = document.getElementById("pageMain");

const  getData = async() =>
{
    const response = await fetch("https://jsonplaceholder.typicode.com/posts/");
    const data = await response.json(); //.json() convert response to usable array

    console.log(data);

    return data;
}


const display = async() =>
{
    const posts = await getData();

    for(let post of posts)
    {
        const div = document.createElement("div");
        
        div.classList.add("post");
        const h2 = document.createElement("h2");
        const p = document.createElement("p");

        h2.innerText = post.title;
        p.innerText = post.body;

        div.appendChild(h2);
        div.appendChild(p);
        pageMain.appendChild(div);
    }

    
}


display();