const submitBtn = document.getElementById("submitBtn");
const overlay = document.querySelector(".overlay");
const overlayBtn = document.getElementById("overlayBtn");
const box = document.querySelector(".box");


submitBtn.addEventListener("click", function(){
    overlay.style.display = "flex";
});

overlay.addEventListener("click",function()
{
    overlay.style.display = "none";
});

overlayBtn.addEventListener("click",function(){
    overlay.style.display = "none";
});